/*variables */
var newPage=0;
var today = new Date();

/*loadData is a function that load the data page by page on scrolling event 
and add element to the view */
function loadData(page=0){
  //load the url with the page in argument
  var url='https://api.rocketfid.com/activity/cache/all/'+ page +'/25/';
  fetch(url,{
    method:"GET",
    headers:{
      'X-Instance': 'rainbow',
      'X-Lang':'fr',
      'X-Widget-Version' : '3.0.1',
      'Content-Type': 'application/json',
    }
  }).then(res=>{  
      res.json().then(data=>{
        //in a loop, add a view for each element of the loaded data
        for(i=0; i<25; i++) {
          //store the data to display in a variable currentdata
          var currentdata =data[i];
          
          /*Create a div which will contain all the information of the currentdata*/
          var activity = document.createElement("DIV");
          activity.className='activity';
          activity.id = currentdata['id'];
          // add the div to the document
          document.getElementById('activityforum').appendChild(activity); 
          
          // Create a button which will allo to load more data of the performer
          const plus =document.createElement("BUTTON");
          plus.className='plusbutton';
          plus.id = currentdata['performer']['id'] + '.' + currentdata['id'];
          plus.innerHTML='+';
          //add the button to the main div of the element
          document.getElementById(activity.id).appendChild(plus);
          //add a click event to the button ! It is important to have a different id for each button!
          document.getElementById(plus.id).addEventListener("click", displayPerformer );

          /*Add the name of the performer */
          const user = document.createElement("DIV");
          user.id = currentdata['performer']['id'];
          user.className='user';
          
          //Not all performer gives a name. 
          if (currentdata['performer']['nickname']===null){
            user.innerHTML = 'INCONNU';
          }
          else{ user.innerHTML = currentdata['performer']['nickname']; }

          // add the name of the performer to the div of the element
          document.getElementById(activity.id).appendChild(user); 

          // Create a element p to display the score of the action/achievement
          const score = document.createElement("P");
          score.className = 'scoreMessage';
          document.getElementById(activity.id).appendChild(score);         


          /* Add the date of the action */
          const date = document.createElement("P");
          date.id=currentdata['performer']['id'];
          date.className = 'dateofcreation';
          stringDate=currentdata['datecreation']['date'].split(' ');
          newDate= new Date(stringDate[0]+'T'+stringDate[1]);
          
          const diffTime = Math.abs(today - newDate);
          const diffDays = Math.ceil(diffTime / (1000 * 60 ));

          //display different text depending on the date
          if (diffDays<=1){
            
            date.innerHTML='à l\'instant';
          } 
          else if(diffDays>1 && diffDays<60){
            
            date.innerHTML=diffDays+' min';
          }
          else if (diffDays>=60 && (diffDays/60)<24 ){
              if(Math.round(diffDays/60)===1){
                date.innerHTML= Math.round(diffDays/60)+' heure';
              }
              else{
              date.innerHTML= Math.round(diffDays/60)+' heures';}
          }
          else if ((diffDays/60)>=24){
            if ((diffDays/(60*24))<2){
              date.innerHTML= 'Hier';
            }
            else{
              date.innerHTML = Math.round(diffDays/(60*24))+' jours';
            }
          }
    

          /*Add the action of the performer as e message to the community */
        
          const message = document.createElement("DIV");   
          message.className='insideMessage';
          message.id = currentdata['performer']['id'];
          
          if (currentdata['action'].length==0){
            if(currentdata['achievement']['name']===[]){
              message.innerHTML = 'pas de message';
              score.innerHTML = '0';
            }
            else{
              message.innerHTML = currentdata['achievement']['name'];
              score.innerHTML = '+'+currentdata['achievement']['score'];
            }
          }
          else{ message.innerHTML = currentdata['action']['name']; 
                score.innerHTML = '+'+currentdata['action']['score'];
        } 

        // Add the message of the action and the date to the main div
        document.getElementById(activity.id).appendChild(message);
        document.getElementById(activity.id).appendChild(date);

      }
    });
  }).catch(error => alert("Erreur:" +error));
  
}

// Call the function to load the first page of data 
loadData();

/* Event listener of scroll event. Will call the loadData function 
when the user is at the end of the page  */
window.addEventListener('scroll', () =>{
  if(window.scrollY + window.innerHeight >= document.documentElement.scrollHeight){
    newPage++;
    loadData(newPage);
  }
})

/*Function that load information of a performer depending on 
his personal ID and display his level and total score */
function displayPerformer(event){

  var id= event.target.id.split('.')[0];
  fetch('https://api.rocketfid.com/user/'+ id +'/',{
    method:"GET",
    headers:{
      'X-Instance': 'rainbow',
      'X-Token':'gOSRX21GoS54ZLkYOADIb1tBG8zkXJuR2w2Duo8qiatDHEwEn3R13Z\\/9\\/2BVKk6Mt9MBy9k8X2i7MrYleYFb7crZSh6O9OyUY3CODmVdQZWcUG99l0SfTe0Ah6Fy4mdVs4iET+Ki1F3YTd6LfNLiCdNNr1AX2pTuoSF9MMnVfC18Lh5R9L7R+bbCLkxxcev27N6aHiN3L2+K1siLi9mP7Dg=',
      'X-Lang':'fr',
      'X-Widget-Version' : '3.0.1',
      'Content-Type': 'application/json',
    }
  }).then(res =>{res.json().then(data=>{

    if(data['nickname']===null){
      document.getElementById('namePerformer').innerHTML = 'Inconnu';
    
    }
    else{
      document.getElementById('namePerformer').innerHTML = data['nickname'];}
  
    document.getElementById('level').innerHTML = 'Niveau : '+ data['expertizes'][0]['levelname']; 
    document.getElementById('score').innerHTML = 'Score : '+data['score']['total'];
    });

  }).catch(error => alert("Erreur:" +error));

}
